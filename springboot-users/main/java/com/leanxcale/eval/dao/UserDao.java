package com.leanxcale.eval.dao;


import com.leanxcale.eval.entity.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, String> {

  List<User> findAll();
}
