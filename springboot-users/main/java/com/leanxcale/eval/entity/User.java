package com.leanxcale.eval.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
public class User {

  @Id
  private String username;
  private byte[] password;

}
