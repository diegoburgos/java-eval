package com.leanxcale.eval;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@PropertySource("classpath:application.properties")
@EnableSwagger2
@EnableScheduling
@EnableAsync
@Log4j2
public class UsersApplication {

  public static void main(String[] args) {
    log.info("Starting E2eProbeApplication...");
    SpringApplication.run(UsersApplication.class, args);
  }

  @PostConstruct
  public void init() {
    // TODO
  }

  @PreDestroy
  public void preDestroy() {
    // TODO
  }

  @Bean
  public Docket productApi() {
    return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.leanxcale"))
        .build();
  }


}
