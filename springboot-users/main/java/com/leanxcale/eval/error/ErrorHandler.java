package com.leanxcale.eval.error;

import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler({Exception.class})
  public ErrorResponse notControlledException(Exception e, HttpServletResponse response) {
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    log.error("Not controlled exception", e);
    return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Not controlled exception");
  }

  @ExceptionHandler({InvalidUserException.class})
  public ErrorResponse invalidClusterException(Exception e, HttpServletResponse response) {
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    log.error("Invalid cluster", e);
    return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage());
  }
}
