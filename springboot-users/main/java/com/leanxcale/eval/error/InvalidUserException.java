package com.leanxcale.eval.error;

public class InvalidUserException extends RuntimeException {

  public InvalidUserException(String msg) {
    super(msg);
  }
}
