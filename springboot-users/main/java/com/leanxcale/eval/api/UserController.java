package com.leanxcale.eval.api;

import com.leanxcale.eval.entity.User;
import com.leanxcale.eval.error.InvalidUserException;
import com.leanxcale.eval.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
@Slf4j
public class UserController {

  @Autowired private UserService userService;

  @RequestMapping(method = RequestMethod.GET, value = "")
  @ApiOperation(value = "Get all users")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 500, message = "Server Generic Error")})
  public ResponseEntity<List<User>> getAllUsers() {
    log.warn("getAllUsers(): get all");
    List<User> list = userService.getAll();
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{username}")
  @ApiOperation(value = "Get a user")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 500, message = "Server Generic Error")})
  public ResponseEntity<User> getUser(
      @ApiParam(value = "username", required = true) @PathVariable("username") String username) {
    log.warn("getUser(): getUser {}", username);
    User user = userService.get(username);
    if (user == null) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    } else {
      return new ResponseEntity<>(user, HttpStatus.OK);
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{username}/{password}")
  @ApiOperation(value = "Change a user password")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 500, message = "Server Generic Error")})
  public ResponseEntity<User> updateUser(
      @ApiParam(value = "username", required = true) @PathVariable("username") String username,
      @ApiParam(value = "password", required = true) @PathVariable("password") String password) {
    log.warn("updateUser(): username {}", username);
    User user = userService.update(username, password);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/{username}/{password}")
  @ApiOperation(value = "Create a new user")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 500, message = "Server Generic Error")})
  public ResponseEntity<User> addUser(
      @ApiParam(value = "username", required = true) @PathVariable("username") String username,
      @ApiParam(value = "password", required = true) @PathVariable("password") String password) {
    log.warn("addUser(): username {} password {}", username, password);
    if (userService.get(username) != null) {
      throw new InvalidUserException("User " + username + " was already created");
    }
    User user = userService.add(username, password);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{username}")
  @ApiOperation(value = "Delete a user")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 500, message = "Server Generic Error")})
  public ResponseEntity<User> deleteUser(
      @ApiParam(value = "username", required = true) @PathVariable("username") String username) {
    log.warn("deleteUser(): username {}", username);
    User user = userService.delete(username);
    if (user != null) {
      return new ResponseEntity<>(user, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }


}
