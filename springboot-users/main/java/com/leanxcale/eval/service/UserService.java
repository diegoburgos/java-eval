package com.leanxcale.eval.service;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.leanxcale.eval.dao.UserDao;
import com.leanxcale.eval.entity.User;
import com.leanxcale.eval.error.InvalidUserException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Slf4j
@Service
public class UserService {

  @Autowired
  private UserDao dao;

  public User add(String username, String password) {
    User user = new User(username, password.getBytes(UTF_8));
    dao.save(user);
    log.info("New User created with User:{} and password:{}", user.getUsername(), Arrays.toString(user.getPassword()));
    return user;
  }

  public User get(String username) {
    Optional<User> opt = dao.findById(username);
    if (opt.isPresent()) {
      return opt.get();
    } else {
      return null;
    }
  }

  public List<User> getAll() {
    List<User> allTheCloudManagers = dao.findAll();
    log.debug("Getting all the Cloud Managers: {}", allTheCloudManagers);
    return allTheCloudManagers;
  }

  public User delete(String username) {
    Optional<User> userOptional = dao.findById(username);
    if (userOptional.isPresent()) {
      User user = userOptional.get();
      dao.delete(user);
      return user;
    }
    return null;
  }

  public User update(String username, String password) {
    Optional<User> cmOptional = dao.findById(username);
    if (cmOptional.isPresent()) {
      User user = cmOptional.get();
      user.setPassword(password.getBytes(UTF_8));
      dao.save(user);
      return user;
    } else {
      throw new InvalidUserException("Cannot update user with username=" + username + " not found");
    }
  }
}
