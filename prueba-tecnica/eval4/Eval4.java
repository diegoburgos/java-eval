import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Eval4 {

  private static final int N_THREADS = 2;

  private int counter = 0;
  private final LinkedList<Integer> items = new LinkedList<>();

  public static void main(String[] args) throws InterruptedException {
    Eval4 my = new Eval4();
    Thread[] ths = new Thread[N_THREADS];
    for (int i=0; i<ths.length; i++) {
      ths[i] = new MyProcess(my);
      ths[i].start();
    }
    for (Thread th : ths) {
      synchronized (th) {
        th.join();
      }
    }

    System.out.println(my.counter);
    System.out.println(my.items);
  }

  private static class MyProcess extends Thread {

    private final Eval4 my;

    public MyProcess(Eval4 my) {
      this.my = my;
    }

    public void run() {
      Random r = ThreadLocalRandom.current();
      for (int j=0; j<100; j++) {
        if (r.nextBoolean()) {
          my.add(my.counter++);
        } else {
          my.remove(my.counter++);
        }
      }
    }
  }

  private void remove(int i) {
    if (items.contains(i)) {
      items.remove((Integer) i);
    }
    System.out.println("removing " + i);
  }

  private void add(int i) {
    if (!items.contains(i)) {
      items.add(i);
    }
    System.out.println("adding " + i);
  }
}
