# Technical Java Evaluation

# Goal
The goal is not to finish all the exercises, but try to do it.

# Scope
| Exercise       | time   |
|----------------|--------|
| SpringBoot JPA | 45 min |
| Pure Java  JPA | 45 min |
## SpringBoot
We provide you a SpringBoot application to manage users. We'd like to add a functionality to disable an use and don't give it access.
The method GET curl localhost:8080/users/hasAccess/{username} should return if the user has access or not.

Swagger is available on [http://localhost:8080/swagger-ui.html#/](http://localhost:8080/swagger-ui.html#/)

## Pure Java
The goal of this set of exercises is to understand your knowledge on Java

| Exercise   | time   |
|------------|--------|
| Exercise 1 | 10 min |
| Exercise 2 | 10 min |
| Exercise 3 | 10 min |
| Exercise 4 | 15 min |

### Exercise 1
The goal of this exercise is to implement a recursive method to print the numbers from 0 until a provided number using recursion

### Exercise 2
The goal of this exercise is to understand why some java code is failing with a given exception

### Exercise 3
The goal of this exercise is to understand why a java program, after some time running, starts consuming al the CPU and, a bit later, crashes. 

### Exercise 4
I have a program that works with 1 single process, why it doesn't work with more than 1 thread?